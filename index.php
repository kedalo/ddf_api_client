<?php
    require_once('configuration.php');
    require_once('functions.php');

    $tourID = getTourID();

    if($tourID) {

        $response = request(
            $config['api_link'] . $config['api_version'] . $config['api_methods']['auth']['value'],
            $config['api_methods']['auth']['request'],
            [
                "Authorization:  " . $config['api_base_token_type'] . " " . base64_encode($config['api_user_password'] . ':' . $config['api_role_name'])
            ]
        );

        if ($response) {
            $clientData = json_decode($response);

            $data = [
                'id' => $tourID,
                'site' => $config['site_name']
            ];
            $dataString = http_build_query($data);

            $response = request(
                $config['api_link'] . $config['api_version'] . $config['api_methods']['getTur']['value'] . '?' . $dataString,
                $config['api_methods']['getTur']['request'],
                [
                    "Authorization:  $clientData->token_type $clientData->access_token"
                ]
            );

            $result = json_decode($response);
            if(isset($result->fail) && $result->fail == true) {
                die('404 Not found.');
            }
        }
        echo $response;
    } else {
        die('404 Not found.');
    }