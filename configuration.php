<?php
    $config = [
        'api_link' => 'http://129769.simplecloud.club/api/',
        'api_version' => 'v1',
        'api_methods' => [
            'auth' => [
                'value' => '/auth/app',
                'request' => 'POST'
            ],
            'getTur' => [
                'value' => '/application-data',
                'request' => 'GET'
            ]
        ],
        'api_user_password' => '1sP_io3g*hGe5kg',
        'api_role_name' => 'tour_manager',
        'api_base_token_type' => 'Basic',
        'site_name' => 'Layout 1'
    ];
